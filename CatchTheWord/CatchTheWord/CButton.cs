﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace CatchTheWord
{
    public class CButton : CSprite
    {
        public string Text = "";
        SpriteFont Font = null;

        public event EventHandler ButtonPressed;
        protected virtual void OnButtonPressed(EventArgs e)
        {
            EventHandler handler = ButtonPressed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public CButton(int x, int y, Texture2D image)
            : base(x, y, image)
        {
        }

        public CButton(int x, int y, Texture2D image, string text, SpriteFont font)
            : base(x, y, image)
        {
            Text = text;
            Font = font;
        }

        /*private bool IsRegionTransparent(MouseState mouse)
        {
            return GetPixel(mouse.X, mouse.Y, ImageWidth) == Color.Transparent;
        }

        public Color GetPixel(int x, int y, int width)
        {
            Color[] colors = GetPixels();
            return colors[x + (y * width)];
        }

        public Color[] GetPixels()
        {
            Color[] colors1D = new Color[ImageWidth * ImageHeight];
            Image.GetData(colors1D);
            return colors1D;
        }*/

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            MouseState mouse = Mouse.GetState();
            Rectangle mouseRect = new Rectangle(mouse.X, mouse.Y, 1, 1);
            if (mouseRect.Intersects(SpriteBounds))
            {
                if (mouse.LeftButton == ButtonState.Pressed) OnButtonPressed(EventArgs.Empty);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if (!Text.Equals(""))
            {
                CGame.DrawString(spriteBatch, Font, Text, SpriteBounds, CGame.EAlignment.Center, Color.White);
            }
        }

        public void SetText(string text, SpriteFont font)
        {
            Text = text;
            Font = font;
        }
    }
}
