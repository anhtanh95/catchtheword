using System;

namespace CatchTheWord
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (CGame game = new CGame())
            {
                game.Run();
            }
        }
    }
#endif
}

