﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using TomShane.Neoforce.Controls;

namespace CatchTheWord
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        CMainMenu mainMenu;
        public CMainGame mainGame;
        public CScore score;
        public CSetting setting;
        public CAbout about;
        public static Manager manager;
        SoundEffect effect;

        public EGameState CurrentGameState = EGameState.MainMenu;
        public static SpriteFont Arial14, Arial12, Andy10, Andy20, Andy25;
        public enum EGameState
        {
            MainMenu,
            GameStarted,
            GamePaused,
            GameOver,
            Score,
            Setting,
            About,
        }

        public CGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            manager = new Manager(this, graphics, "Blue");
            manager.SkinDirectory = Path.Combine(System.Environment.CurrentDirectory, "Content");
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            manager.Initialize();
            IsMouseVisible = true;

            string dirPath = Path.Combine(System.Environment.CurrentDirectory, "Vocabulary");
            if (!Directory.Exists(dirPath))
            {
                ShowErrorMsg("Can not found 'Vocabulary' folder!!");
            }
            else if (!File.Exists(Path.Combine(dirPath, "Basic1000.txt")))
            {
                ShowErrorMsg("Can not found 'Basic1000.txt' file!!");
            }
            else
            {
                mainMenu.Init();

                //Loading vocabulary
                Dictionary<string, string[]> basic1000 = new Dictionary<string, string[]>();
                using (FileStream stream = File.Open(Path.Combine(dirPath, "Basic1000.txt"), FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] word = line.Split('|');
                            string[] sub = word[1].Split(',');
                            basic1000.Add(word[0], sub);
                        }
                    }
                }
                CMainGame.VocabularyLibary = new Dictionary<EVocabularyType, Dictionary<string, string[]>>();
                CMainGame.VocabularyLibary.Add(EVocabularyType.Basic1000, basic1000);
            }
            CScore.InitialHighScores();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Arial12 = Content.Load<SpriteFont>("Arial12");
            Arial14 = Content.Load<SpriteFont>("Arial14");
            Andy10 = Content.Load<SpriteFont>("Andy10");
            Andy20 = Content.Load<SpriteFont>("Andy20");
            Andy25 = Content.Load<SpriteFont>("Andy25");

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;
            //graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            mainMenu = new CMainMenu(this, graphics.GraphicsDevice);
            mainGame = new CMainGame(this, graphics.GraphicsDevice);
            score = new CScore(this, graphics.GraphicsDevice);
            setting = new CSetting(this, graphics.GraphicsDevice);
            about = new CAbout(this, graphics.GraphicsDevice);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (CurrentGameState)
            {
                case EGameState.MainMenu:
                    mainMenu.Update(gameTime);
                    break;
                case EGameState.GameStarted:
                case EGameState.GamePaused:
                    mainGame.Update(gameTime);
                    break;
                case EGameState.Score:
                    score.Update(gameTime);
                    break;
                case EGameState.Setting:
                    setting.Update(gameTime);
                    break;
                case EGameState.About:
                    about.Update(gameTime);
                    break;
            }

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            manager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            manager.BeginDraw(gameTime);
            spriteBatch.Begin();
            switch (CurrentGameState)
            {
                case EGameState.MainMenu:
                    mainMenu.Draw(spriteBatch);
                    break;
                case EGameState.GameStarted:
                case EGameState.GamePaused:
                    mainGame.Draw(spriteBatch);
                    break;
                case EGameState.Score:
                    score.Draw(spriteBatch);
                    break;
                case EGameState.Setting:
                    setting.Draw(spriteBatch);
                    break;
                case EGameState.About:
                    about.Draw(spriteBatch);
                    break;
            }
            spriteBatch.End();
            manager.EndDraw();

            base.Draw(gameTime);
        }

        public enum EAlignment { Center = 0, Left = 1, Right = 2, Top = 4, Bottom = 8 }

        public static void DrawString(SpriteBatch spriteBatch, SpriteFont font, string text, Rectangle bounds, EAlignment align, Color color)
        {
            Vector2 size = font.MeasureString(text);
            Vector2 pos = new Vector2(bounds.Center.X, bounds.Center.Y);
            Vector2 origin = size * 0.5f;
            if (align.HasFlag(EAlignment.Left))
                origin.X += bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(EAlignment.Right))
                origin.X -= bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(EAlignment.Top))
                origin.Y += bounds.Height / 2 - size.Y / 2;

            if (align.HasFlag(EAlignment.Bottom))
                origin.Y -= bounds.Height / 2 - size.Y / 2;

            spriteBatch.DrawString(font, text, pos, color, 0, origin, 1, SpriteEffects.None, 0);
        }

        private void ShowErrorMsg(string errorMsg)
        {
            // Create and setup Window control.
            Window window = new Window(manager);
            window.Init();
            window.Text = "Error!";
            window.Width = 400;
            window.Height = 124;
            window.Left = (graphics.GraphicsDevice.Viewport.Width - window.Width) / 2;
            window.Top = 150;
            window.Resizable = false;
            window.Movable = false;
            window.Closed += new WindowClosedEventHandler(window_Closed);

            // Create Button control and set the previous window as its parent.
            Button button = new Button(manager);
            button.Init();
            button.Text = "OK";
            button.Width = 72;
            button.Height = 24;
            button.Left = (window.ClientWidth - button.Width) / 2;
            button.Top = window.ClientHeight - button.Height - 8;
            button.Parent = window;
            button.Click += new EventHandler(button_Click);

            Label label = new Label(manager);
            label.Init();
            label.Left = 80;
            label.Top = 20;
            label.Text = errorMsg;
            label.Width = label.Text.Length * 6;
            label.Parent = window;
            manager.Add(window);
        }

        private void window_Closed(object sender, WindowClosedEventArgs e)
        {
            Exit();
        }

        private void button_Click(object sender, EventArgs e)
        {
            Exit();
        }
    }
}
