﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatchTheWord
{
    class CBarrow
    {
        public CSprite Barrow, Arrow;
        public CButton Board;
        public bool HaveArrow = true;

        public CBarrow(ContentManager content, string text, EBoardStyle style)
        {
            Texture2D boardImage = content.Load<Texture2D>("GameContent/" + GetName(style));
            Board = new CButton(348, 458, boardImage, text, CGame.Arial12);

            Texture2D barrowImage = content.Load<Texture2D>("GameContent/obj_barrow");
            Barrow = new CSprite(361, 508, barrowImage);

            Texture2D arrowImage = content.Load<Texture2D>("GameContent/bt_arrow");
            Arrow = new CSprite(390, 490, arrowImage);
        }

        float elapsed = 0;
        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                if (Board.Location.X > 0)
                {
                    Board.SetPosition(Board.Location.X - 6, Board.Location.Y);
                    Barrow.SetPosition(Barrow.Location.X - 6, Barrow.Location.Y);
                    if (HaveArrow)
                        Arrow.SetPosition(Arrow.Location.X - 6, Arrow.Location.Y);
                }
                else
                {
                    Board.SetPosition(0, Board.Location.Y);
                    Barrow.SetPosition(13, Barrow.Location.Y);
                    if (HaveArrow)
                        Arrow.SetPosition(42, Arrow.Location.Y);
                }
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                if (Board.Location.X < 696)
                {
                    Board.SetPosition(Board.Location.X + 6, Board.Location.Y);
                    Barrow.SetPosition(Barrow.Location.X + 6, Barrow.Location.Y);
                    if (HaveArrow)
                        Arrow.SetPosition(Arrow.Location.X + 6, Arrow.Location.Y);
                }
                else
                {
                    Board.SetPosition(696, Board.Location.Y);
                    Barrow.SetPosition(696 + 13, Barrow.Location.Y);
                    if (HaveArrow)
                        Arrow.SetPosition(696 + 42, Arrow.Location.Y);
                }
            }

            elapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if ((Keyboard.GetState().IsKeyDown(Keys.Enter) || Keyboard.GetState().IsKeyDown(Keys.Space)) 
                && HaveArrow && elapsed >= 1000)
            {
                HaveArrow = false;
                elapsed = 0;
                CMainGame.effectShoot.Play();
            }
            else if (!HaveArrow)
            {
                Arrow.Location.Y -= 10;
                if (Arrow.Location.Y < -40)
                {
                    ReloadArrow();
                }
            }
            Board.Update(gameTime);
            Barrow.Update(gameTime);
            Arrow.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Barrow.Draw(spriteBatch);
            Board.Draw(spriteBatch);
            Arrow.Draw(spriteBatch);
        }

        public void ReloadArrow()
        {
            HaveArrow = true;
            Arrow.SetPosition(Board.Location.X + 42, 490);
        }

        public enum EBoardStyle
        {
            GreenBoard, 
        }

        public static string GetName(EBoardStyle style)
        {
            switch (style)
            {
                case EBoardStyle.GreenBoard:
                    return "obj_board";
                default:
                    return "obj_board";
            }
        }
    }
}
