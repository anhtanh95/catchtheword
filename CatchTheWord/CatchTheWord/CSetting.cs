﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Media;
using TomShane.Neoforce.Controls;

namespace CatchTheWord
{
    public class CSetting
    {
        private CGame Main;
        GraphicsDevice GraphicsDevice;
        CButton VolumneButton, ModeButton, backButton, checkboxOnButton, checkboxOffButton, checkEasyButton, checkHardButton;
        SpriteBatch spriteBatch;
        Texture2D checkOnImage, checkOffImage;


        public static bool on = false;

        public void Init()
        {
            Texture2D backImage = Main.Content.Load<Texture2D>("bt_back");
            int x_offset = 5;
            int y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 5;
            backButton = new CButton(x_offset, y_offset, backImage);
            backButton.ButtonPressed += new System.EventHandler(BackButton_Click);

            checkOnImage = Main.Content.Load<Texture2D>("SettingContent/bt_switch_on");
            checkOffImage = Main.Content.Load<Texture2D>("SettingContent/bt_switch_off");
            int xc = 330;
            int yc = 120;
            checkboxOnButton = new CButton(xc, yc, checkOnImage);
            checkboxOnButton.ButtonPressed += new System.EventHandler(CheckboxOnButton_Click);

            Texture2D checkBoxEasyImage = Main.Content.Load<Texture2D>("SettingContent/cb_select_true");

            int x = 350;
            int y = 230;
            checkEasyButton = new CButton(x , y, checkBoxEasyImage);
            checkEasyButton.ButtonPressed += new System.EventHandler(CheckEasyButton_Click);


            Texture2D checkBoxHardImage = Main.Content.Load<Texture2D>("SettingContent/cb_select_false");
            int xh = 450;
            int yh = 230;
            checkHardButton = new CButton(xh, yh, checkBoxHardImage);
            checkHardButton.ButtonPressed += new System.EventHandler(CheckHardButton_Click);

            //CGame.manager

            /*Button HardButton = new Button(CGame.manager);
            HardButton.Init();
            HardButton.Top = 160;
            HardButton.Left = 333;
            HardButton.Width = 50;
            HardButton.Height = 50;
            HardButton.Glyph = new Glyph(checkImage, new Rectangle(0,0,50,50));
            CGame.manager.Add(HardButton);*/
        }

        public CSetting(CGame game2, GraphicsDevice gd)
        {

            Main = game2;
            GraphicsDevice = gd;
        }

        public void Update(GameTime gameTime)
        {
            backButton.Update(gameTime);
         
            checkboxOnButton.Update(gameTime);

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.Content.Load<Texture2D>("SettingContent/bg_setting"), new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            // spriteBatch.Draw(Main.Content.Load<Texture2D>("SettingContent/bt_switch_on"), new Rectangle(300, 140,50,50), Color.White);
            backButton.Draw(spriteBatch);

            CGame.DrawString(spriteBatch, CGame.Andy25, "Setting", new Rectangle(250, 100, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "#Sound:", new Rectangle(225, 150, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "#Level:", new Rectangle(225, 200, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "Easy", new Rectangle(300, 250, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "Hard", new Rectangle(400, 250, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "#Thematic:  1000 Bassic Vocabulary", new Rectangle(225, 300, 0, 0), CGame.EAlignment.Left, Color.Black);
            checkEasyButton.Draw(spriteBatch);
            checkHardButton.Draw(spriteBatch);
            checkboxOnButton.Draw(spriteBatch);

        }

        private void BackButton_Click(object sender, System.EventArgs e)
        {
            while(CGame.manager.Controls.Count() > 0)
            {
                CGame.manager.Remove(CGame.manager.Controls.First());
            }
            Main.CurrentGameState = CGame.EGameState.MainMenu;
        }

        private void CheckboxOnButton_Click(object sender, System.EventArgs e)
        {
            if (on)
            {
                on = true;
            }
            else
            {
                on = false;
            }
        }

        private void CheckHardButton_Click(object sender, System.EventArgs e)
        {
        }

        private void CheckEasyButton_Click(object sender, System.EventArgs e)
        {
        }
    }
}
