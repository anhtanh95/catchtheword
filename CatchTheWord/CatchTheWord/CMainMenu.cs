﻿using Microsoft.Xna.Framework.Graphics;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Media;
using System.Collections.Generic;

namespace CatchTheWord
{
    class CMainMenu
    {
        private CGame Main;
        CButton playButton, settingButton, scoreButton, aboutButton, exitButton;
        List<CButton> buttons = new List<CButton>();
        GraphicsDevice GraphicsDevice;

        public CMainMenu(CGame game, GraphicsDevice gd)
        {
            Main = game;
            GraphicsDevice = gd;
        }

        public void Init()
        {
            Texture2D buttonImage = Main.Content.Load<Texture2D>("bt_menu");
            int x_offset = (GraphicsDevice.Viewport.Width - buttonImage.Width) / 2;
            int y_offset = 120;
            playButton = new CButton(x_offset, y_offset, buttonImage, "Play", CGame.Arial14);
            playButton.ButtonPressed += new EventHandler(PlayButton_Click);
            buttons.Add(playButton);

            y_offset += playButton.Image.Height + 10;
            scoreButton = new CButton(x_offset, y_offset, buttonImage, "Score", CGame.Arial14);
            scoreButton.ButtonPressed += new EventHandler(ScoreButton_Click);
            buttons.Add(scoreButton);

            y_offset += scoreButton.Image.Height + 10;
            settingButton = new CButton(x_offset, y_offset, buttonImage, "Setting", CGame.Arial14);
            settingButton.ButtonPressed += new EventHandler(SettingButton_Click);
            buttons.Add(settingButton);

            y_offset += settingButton.Image.Height + 10;
            aboutButton = new CButton(x_offset, y_offset, buttonImage, "About", CGame.Arial14);
            aboutButton.ButtonPressed += new EventHandler(AboutButton_Click);
            buttons.Add(aboutButton);

            y_offset += aboutButton.Image.Height + 10;
            exitButton = new CButton(x_offset, y_offset, buttonImage, "Exit", CGame.Arial14);
            exitButton.ButtonPressed += new EventHandler(ExitButton_Click);
            buttons.Add(exitButton);
        }

        
        public void Update(GameTime gameTime)
        {
            foreach (CButton button in buttons)
            {
                if (button != null)
                button.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.Content.Load<Texture2D>("GameContent/bg_game"), new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            foreach (CButton button in buttons)
            {
                if (button != null)
                    button.Draw(spriteBatch);
            }
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            Main.mainGame.Init();//Khởi tạo màn chơi
            Main.CurrentGameState = CGame.EGameState.GameStarted;//Chuyển state
        }

        public void ScoreButton_Click(object sender, EventArgs e)
        {
            HighScoreData data = new HighScoreData(5);
            data = CScore.LoadHighScores();

            Main.score.Init();
            Main.score.ScoreData = data;

            Main.CurrentGameState = CGame.EGameState.Score;
        }

        public void SettingButton_Click(object sender, EventArgs e)
        {
            Main.setting.Init();
            Main.CurrentGameState = CGame.EGameState.Setting;
        }
        private void AboutButton_Click(object sender, EventArgs e)
        {
            Main.about.Init();
            Main.CurrentGameState = CGame.EGameState.About;
        }
        
        private void ExitButton_Click(object sender, EventArgs e)
        {
            Main.Exit();
        }
    }
}
