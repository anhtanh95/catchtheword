﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace CatchTheWord
{
    public class CEnemy
    {
        public CSprite butterfly;
        public string TextBoard;
        CButton board_fly;
        int MobId;

        public CEnemy(int id, int x, int y, int direction, string text)
        {
            MobId = id;
            TextBoard = text;
            butterfly = new CSprite(x, y, CMainGame.EnemyImage);
            butterfly.FrameCount = 2;
            butterfly.FrameImage = new Rectangle(0, 0, 63, 63);
            butterfly.Direction = direction;
            butterfly.SetState((int)CMainGame.ECharacterState.Walk);

            board_fly = new CButton(x - 30, y + CMainGame.EnemyImage.Height
                , CMainGame.EnemyBoard
                , text
                , CGame.Arial12);
        }

        public int GetMobId()
        {
            return MobId;
        }

        float elapsed = 0;
        public void Update(GameTime gameTime)
        {
            //Xử lý ảnh động cho butterfly, delay frame rate = 150 miliseconds
            elapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (butterfly.State == 1 && elapsed >= 150f) // means walking state
            {
                if (butterfly.Frame >= 1)
                    butterfly.Frame = 0;
                else butterfly.Frame++;
                elapsed = 0;
                butterfly.FrameImage = new Rectangle(63 * butterfly.Frame, 0, 63, 63);
            }
            //Xử lý di chuyển cho Butterfly
            butterfly.Location.X = butterfly.Location.X + (butterfly.Direction == 1 ? 1 : -1);
            butterfly.Update(gameTime);
            //Xử lý di chuyển cho Board
            float newX_Pos = board_fly.Location.X + (butterfly.Direction == 1 ? 1 : -1);
            board_fly.SetPosition(newX_Pos, board_fly.Location.Y);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(butterfly.Image, butterfly.Location, butterfly.FrameImage
                , Color.White, 0f
                , Vector2.Zero, 1f
                , (SpriteEffects)butterfly.Direction
                , 0);
            board_fly.Draw(spriteBatch);
        }

        public Vector2 GetLocation()
        {
            return butterfly.Location;
        }
    }
}
