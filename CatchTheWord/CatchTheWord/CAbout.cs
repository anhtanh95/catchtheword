﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml.Serialization;
using System.IO;

namespace CatchTheWord
{
    public class CAbout
    {
        private CGame Main;
        GraphicsDevice GraphicsDevice;
        CButton backButton;
        SpriteBatch spriteBatch;
        public void Init()
        {
            Texture2D backImage = Main.Content.Load<Texture2D>("bt_back");
            int x_offset = 5;
            int y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 5;
            backButton = new CButton(x_offset, y_offset, backImage);
            backButton.ButtonPressed += new System.EventHandler(BackkButton_Click);
        }
        public CAbout(CGame game1, GraphicsDevice gd)
        {
            Main = game1;
            GraphicsDevice = gd;
        }


        public void Update(GameTime gameTime)
        {
            backButton.Update(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.Content.Load<Texture2D>("bg_about"), new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            CGame.DrawString(spriteBatch, CGame.Andy25, "About", new Rectangle(250, 100, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "#Game Version:  1.2", new Rectangle(225, 150, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "#Authors: ", new Rectangle(225, 200, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "-Đặng Trần Tiến", new Rectangle(300, 250, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "-Lê Minh Nghị", new Rectangle(300, 280, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "-Trịnh Phương Hoa", new Rectangle(300, 310, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "-Đặng Văn Mạnh", new Rectangle(300, 340, 0, 0), CGame.EAlignment.Left, Color.Black);
            CGame.DrawString(spriteBatch, CGame.Andy20, "-Nguyễn Văn Doanh", new Rectangle(300, 370, 0, 0), CGame.EAlignment.Left, Color.Black);

            backButton.Draw(spriteBatch);
        }
        private void BackkButton_Click(object sender, EventArgs e)
        {
            Main.CurrentGameState = CGame.EGameState.MainMenu;
        }
    }
}
