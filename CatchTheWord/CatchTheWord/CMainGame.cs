﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using TomShane.Neoforce.Controls;

namespace CatchTheWord
{
    public class CMainGame
    {
        private CGame Main;
        CButton backButton, pauseButton, volumeplayButton, volumeoffButton;
        CBarrow barrow;
        GraphicsDevice GraphicsDevice;
        public static Dictionary<int, CEnemy> enemies;
        public static Dictionary<EVocabularyType, Dictionary<string, string[]>> VocabularyLibary;
        public static EVocabularyType currentThemetic;
        public static Texture2D EnemyImage, EnemyBoard;
        CMobGenerator mobGen;
        Button okButton;
        TextBox textBox;
        int Score;

        public static SoundEffect effectShoot, soundShootTrue, soundShootFail;

        Song song;

        public enum ECharacterState
        {
            Stand = 0,
            Walk = 1,
        }

        public CMainGame(CGame game1, GraphicsDevice gd)
        {
            Main = game1;
            GraphicsDevice = gd;
            enemies = new Dictionary<int, CEnemy>();

            //Setup default Themetic for the game
            currentThemetic = EVocabularyType.Basic1000;
        }

        public void Init()
        {
            Texture2D backImage = Main.Content.Load<Texture2D>("bt_back");
            int x_offset = 5;
            int y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 5;
            backButton = new CButton(x_offset, y_offset, backImage);
            backButton.ButtonPressed += new System.EventHandler(BackButton_Click);

            Texture2D pauseImage = Main.Content.Load<Texture2D>("GameContent/bt_pause");
            x_offset += backButton.Image.Width + 5;
            pauseButton = new CButton(x_offset, y_offset, pauseImage);
            pauseButton.ButtonPressed += new System.EventHandler(PauseButton_Click);

            Texture2D volumeplayImage = Main.Content.Load<Texture2D>("GameContent/bt_volume_play");
            x_offset += pauseButton.Image.Width + 3;
            y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 15;
            volumeplayButton = new CButton(x_offset, y_offset, volumeplayImage);
            volumeplayButton.ButtonPressed += new System.EventHandler(VolumeplayButton_Click);

            Texture2D volumeoffImage = Main.Content.Load<Texture2D>("GameContent/bt_volume_off");
            x_offset += pauseButton.Image.Width + 8;
            y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 5;
            volumeoffButton = new CButton(x_offset, y_offset, volumeoffImage);
            volumeoffButton.ButtonPressed += new System.EventHandler(VolumeoffButton_Click);

            soundShootTrue = Main.Content.Load<SoundEffect>("SoundTrue");
            effectShoot = Main.Content.Load<SoundEffect>("SoundShoot");
            soundShootFail = Main.Content.Load<SoundEffect>("SoundFail");
            song = Main.Content.Load<Song>("Song");
            MediaPlayer.Play(song);
            EnemyImage = Main.Content.Load<Texture2D>("GameContent/obj_butterfly");
            EnemyBoard = Main.Content.Load<Texture2D>("GameContent/obj_board_fly");

            //Create/Clear enemies list
            enemies = new Dictionary<int, CEnemy>();

            //Create Mob Generator
            mobGen = new CMobGenerator();
            mobGen.ResetSpawnPos();
            mobGen.GanerateWords();
            mobGen.Start();

            //Create first enemy
            CEnemy enemy = mobGen.Generate(mobGen.rndGen.Next(0, mobGen.MaxMob));
            enemies.Add(enemy.GetMobId(), enemy);

            //Create the Barrow
            barrow = new CBarrow(Main.Content, mobGen.GetRandomMeaning(), CBarrow.EBoardStyle.GreenBoard);

            //Set Score
            Score = 0;
        }

        internal void Update(GameTime gameTime)
        {
            if (Main.CurrentGameState != CGame.EGameState.GamePaused)
            {
                barrow.Update(gameTime);

                List<CEnemy> tobeRemove = new List<CEnemy>();

                foreach (KeyValuePair<int, CEnemy> enemyInfo in enemies)
                {
                    enemyInfo.Value.Update(gameTime);

                    //Check collision
                    if (barrow.Arrow.SpriteBounds.Intersects(enemyInfo.Value.butterfly.SpriteBounds))
                    {
                        tobeRemove.Add(enemyInfo.Value);
                        barrow.ReloadArrow();
                        List<string> meaningList = new List<string>(mobGen.GetMeaningByWord(enemyInfo.Value.TextBoard));
                        if (meaningList.Contains(barrow.Board.Text))
                        {
                            Score++;
                            soundShootTrue.Play();
                        }
                        else
                        {
                            soundShootFail.Play();
                            Main.CurrentGameState = CGame.EGameState.GamePaused;
                            GameOver();
                        }
                    }
                    else if (enemyInfo.Value.butterfly.Direction == 1 && enemyInfo.Value.GetLocation().X >= 800)
                    {
                        tobeRemove.Add(enemyInfo.Value);
                    }
                    else if (enemyInfo.Value.butterfly.Direction == 0 && enemyInfo.Value.GetLocation().X <= -63)
                    {
                        tobeRemove.Add(enemyInfo.Value);
                    }
                }

                bool generateNewWord = false;
                foreach (CEnemy enemyRemove in tobeRemove)
                {
                    //Get new word for barrow's board
                    List<string> meaningList = new List<string>(mobGen.GetMeaningByWord(enemyRemove.TextBoard));
                    if (meaningList.Contains(barrow.Board.Text))
                    {
                        generateNewWord = true;
                    }

                    //Remove enemy
                    enemies.Remove(enemyRemove.GetMobId());

                    //Clear spawn postition
                    int pos = ((((int)enemyRemove.GetLocation().Y / 50) - 1) / 2);
                    mobGen.EmptyPos[pos] = true;

                    //Remove word from screen
                    mobGen.AppearedWords.Remove(enemyRemove.TextBoard);
                }
                mobGen.GanerateWords();
                if (generateNewWord)
                    barrow.Board.Text = mobGen.GetRandomMeaning();

                CEnemy enemy = mobGen.GenerateInteval(gameTime, 2000f);
                if (enemy != null) enemies.Add(enemy.GetMobId(), enemy);
            }

            backButton.Update(gameTime);
            pauseButton.Update(gameTime);
            volumeplayButton.Update(gameTime);
            volumeoffButton.Update(gameTime);
        }

        internal void Draw(SpriteBatch spriteBatch)
        {
            //Draw the background
            spriteBatch.Draw(Main.Content.Load<Texture2D>("GameContent/bg_game"), new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);

            //Draw score
            spriteBatch.DrawString(CGame.Andy20, "Điểm:" + Score, new Vector2(10, 10), Color.Yellow);

            //Draw all buttons
            backButton.Draw(spriteBatch);
            pauseButton.Draw(spriteBatch);
            volumeplayButton.Draw(spriteBatch);
            volumeoffButton.Draw(spriteBatch);

            //Draw the barrow
            barrow.Draw(spriteBatch);

            //Draw all enemies
            foreach (KeyValuePair<int, CEnemy> enemy in enemies)
            {
                enemy.Value.Draw(spriteBatch);
            }
        }

        public void GameOver()
        {
            Panel panel = new Panel(CGame.manager);
            panel.Init();
            panel.Width = 600;
            panel.Height = 200;
            panel.Top = 150;
            panel.Left = (GraphicsDevice.Viewport.Width - panel.Width) / 2;

            //Game Over Title
            Label gameOverLabel = new Label(CGame.manager);
            gameOverLabel.Init();
            gameOverLabel.Skin.Layers[0].Text.Font.Resource = CGame.Arial14;
            gameOverLabel.Text = "GAME OVER";
            gameOverLabel.TextColor = Color.White;
            Vector2 gameOverLabelSize = CGame.Arial14.MeasureString(gameOverLabel.Text);
            gameOverLabel.Width = (int)gameOverLabelSize.X;
            //gameOverLabel.Height = (int)gameOverLabelSize.Y;
            gameOverLabel.Top = 10;
            gameOverLabel.Left = (panel.Width - gameOverLabel.Width) / 2;
            gameOverLabel.Parent = panel;

            //Correct Answer Title
            Label correctAnswerLabel = new Label(CGame.manager);
            correctAnswerLabel.Init();
            correctAnswerLabel.Skin.Layers[0].Text.Font.Resource = CGame.Arial14;
            correctAnswerLabel.Text = "-The correct answer is:";
            correctAnswerLabel.TextColor = Color.White;
            Vector2 correctAswerLabelSize = CGame.Arial14.MeasureString(correctAnswerLabel.Text);
            correctAnswerLabel.Width = (int)correctAswerLabelSize.X;
            correctAnswerLabel.Height = (int)correctAswerLabelSize.Y;
            correctAnswerLabel.Top = gameOverLabel.Top + gameOverLabel.Height;
            correctAnswerLabel.Left = 10;
            correctAnswerLabel.Parent = panel;

            int topPos = correctAnswerLabel.Top + correctAnswerLabel.Height;
            //Correct Answer
            foreach (KeyValuePair<string, string[]> word in mobGen.GetWordByMeaning(barrow.Board.Text))
            {
                Label answerLabel = new Label(CGame.manager);
                answerLabel.Init();
                answerLabel.Skin.Layers[0].Text.Font.Resource = CGame.Arial14;
                string text = word.Key + ":";
                for (int i = 0; i < word.Value.Length; i++)
                {
                    text += word.Value[i] + ((i == word.Value.Length - 1) ? "" : ",");
                }
                answerLabel.Text = text;
                answerLabel.TextColor = Color.White;
                Vector2 aswerLabelSize = CGame.Arial14.MeasureString(answerLabel.Text);
                answerLabel.Width = (int)aswerLabelSize.X;
                answerLabel.Height = (int)aswerLabelSize.Y;
                answerLabel.Top = topPos + 10;
                topPos += answerLabel.Height;
                answerLabel.Left = 15;
                answerLabel.Parent = panel;
            }

            //Enter Player Name Title
            Label enterNameLabel = new Label(CGame.manager);
            enterNameLabel.Init();
            enterNameLabel.Skin.Layers[0].Text.Font.Resource = CGame.Arial14;
            enterNameLabel.Text = "-Enter player name(maximum 12 characters):";
            enterNameLabel.TextColor = Color.White;
            Vector2 enterNameLabelSize = CGame.Arial14.MeasureString(enterNameLabel.Text);
            enterNameLabel.Width = (int)enterNameLabelSize.X;
            enterNameLabel.Height = (int)enterNameLabelSize.Y;
            enterNameLabel.Top = topPos + 10;
            topPos += enterNameLabel.Height;
            enterNameLabel.Left = 10;
            enterNameLabel.Parent = panel;

            textBox = new TextBox(CGame.manager);
            textBox.Init();
            textBox.TextColor = Color.White;
            textBox.Width = 100;
            textBox.Top = topPos + 10;
            textBox.Left = 10;
            textBox.Text = "Player1";
            textBox.Parent = panel;
            textBox.TextChanged += new EventHandler(textBox_TextChanged);

            okButton = new Button(CGame.manager);
            okButton.Init();
            okButton.Width = 100;
            okButton.Height = 40;
            okButton.Top = 150;
            okButton.Left = (panel.Width - okButton.Width) / 2;
            okButton.Text = "Save and Back";
            okButton.Parent = panel;
            okButton.Click += new EventHandler(okButton_Click);

            CGame.manager.Add(panel);
        }

        private void backToMainMenu()
        {
            while (CGame.manager.Controls.Count() > 0)
            {
                CGame.manager.Remove(CGame.manager.Controls.First());
            }
            Main.CurrentGameState = CGame.EGameState.MainMenu;
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (textBox.Text.Length > 12)
            {
                textBox.Text = textBox.Text.Remove(textBox.Text.Length - 1);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            backToMainMenu();
            Main.score.SaveHighScore(textBox.Text, Score);
        }

        private void BackButton_Click(object sender, System.EventArgs e)
        {
            backToMainMenu();
        }

        private void PauseButton_Click(object sender, System.EventArgs e)
        {
            if (Main.CurrentGameState == CGame.EGameState.GameStarted)
            {
                Main.CurrentGameState = CGame.EGameState.GamePaused;
            }
            else
            {
                Main.CurrentGameState = CGame.EGameState.GameStarted;
            }
        }

        private void VolumeplayButton_Click(object sender, System.EventArgs e)
        {

        }
        private void VolumeoffButton_Click(object sender, System.EventArgs e)
        {
        }
    }
}
