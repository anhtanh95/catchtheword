﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CatchTheWord
{
    public class CSprite
    {
        public Vector2 Location;
        public Texture2D Image;
        public int State;
        public Rectangle FrameImage, SpriteBounds;
        public int Frame;
        public int FrameCount;
        public int Direction;//0=Left; 1=Right

        public CSprite() { }

        ~CSprite() { }

        public CSprite(int x, int y, Texture2D image)
        {
            Location = new Vector2(x, y);
            Image = image;
            Frame = 0;
            FrameCount = 1;
            Direction = 0;
            SpriteBounds = new Rectangle(x, y, image.Width / FrameCount, image.Height);
        }

        public void SetState(int state)
        {
            State = state;
            Frame = 0;//reset Frame when change State
        }

        public virtual void Update(GameTime gameTime)
        {
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Image.Width / FrameCount, Image.Height);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, new Rectangle((int)Location.X, (int)Location.Y, Image.Width, Image.Height), Color.White);
        }

        public virtual void SetPosition(float x, float y)
        {
            Location.X = x;
            Location.Y = y;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Image.Width / FrameCount, Image.Height);
        }
    }
}
