﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatchTheWord
{
    [Serializable]
    public struct HighScoreData
    {
        public string[] PlayerName;
        public DateTime[] Date;
        public int[] Score;

        public int Count;

        public HighScoreData(int count)
        {
            PlayerName = new string[count];
            Date = new DateTime[count];
            Score = new int[count];

            Count = count;
        }
    }
}
