﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace CatchTheWord
{
    public class CMobGenerator
    {
        public Random rndGen = new Random();
        public bool[] EmptyPos;
        int mobId;
        bool started;
        public int MaxMob;
        public int MaxAppearedWords;
        public Dictionary<string, string[]> AppearedWords = new Dictionary<string, string[]>();

        public CMobGenerator()
        {
            mobId = 0;
            MaxMob = 4;
            MaxAppearedWords = 5;
            ResetSpawnPos();
        }

        public void Start()
        {
            started = true;
        }

        public void Stop()
        {
            started = false;
        }

        public void ResetSpawnPos()
        {
            EmptyPos = new bool[4] { true, true, true, true };
        }

        float elapsed = 0;
        public CEnemy GenerateInteval(GameTime gameTime, float inteval)
        {
            elapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (started && elapsed >= inteval)
            {
                int pos = rndGen.Next(0, MaxMob);//random an emptypos
                //result will be in range: {0, 1, 2, 3}
                if (!EmptyPos[pos])//check emptypos is empty
                {
                    elapsed = inteval;//continue loop until 
                }
                else
                {
                    elapsed = 0;//reset elapsed time
                    return Generate(pos);//spawn enemy in empty pos
                }
            }
            return null;
        }

        public CEnemy Generate(int emptyPos)
        {
            if (CMainGame.enemies.Count < MaxMob && EmptyPos[emptyPos])
            {
                EmptyPos[emptyPos] = false;
                mobId++;
                int direction = rndGen.Next(0, 2);//random 0 or 1
                KeyValuePair<string, string[]> word = GanerateWord();//get random 1 word
                int x_pos = direction == 1 ? (-63) : 863;//get x position by direction
                int y_pos = 50 * (emptyPos * 2 + 1);//get y postion by empty pos
                return new CEnemy(mobId, x_pos, y_pos, direction, word.Key);
            }
            return null;
        }

        public KeyValuePair<string, string[]> GanerateWord()
        {
            List<string> spawned = new List<string>();
            foreach (CEnemy enemy in CMainGame.enemies.Values)
            {
                spawned.Add(enemy.TextBoard);
            }
            KeyValuePair<string, string[]> word = AppearedWords.ElementAt(rndGen.Next(0, AppearedWords.Count));
            while (spawned.Contains(word.Key))
            {
                word = AppearedWords.ElementAt(rndGen.Next(0, AppearedWords.Count));
            }
            return word;
        }

        public void GanerateWords()
        {
            Dictionary<string, string[]> wordList = CMainGame.VocabularyLibary[CMainGame.currentThemetic];
            KeyValuePair<string, string[]> word = new KeyValuePair<string, string[]>();
            while (AppearedWords.Count < MaxAppearedWords)
            {
                word = wordList.ElementAt(rndGen.Next(0, wordList.Count));
                if (!AppearedWords.Contains(word))
                {
                    AppearedWords.Add(word.Key, word.Value);
                }
            }
        }

        public string GetRandomMeaning()
        {
            string [] meaning = AppearedWords.ElementAt(rndGen.Next(0, AppearedWords.Count)).Value;
            return meaning[rndGen.Next(0, meaning.Length)];
        }

        public string[] GetMeaningByWord(string word)
        {
            return AppearedWords[word];
        }

        public List<KeyValuePair<string, string[]>> GetWordByMeaning(string meaning)
        {
            List<KeyValuePair<string, string[]>> result = new List<KeyValuePair<string, string[]>>();
            foreach (KeyValuePair<string, string[]> word in CMainGame.VocabularyLibary[CMainGame.currentThemetic])
            {
                if (word.Value.Contains(meaning))
                {
                    result.Add(word);
                }
            }
            return result;
        }
    }
}
