﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Xml.Serialization;
using System.IO;

namespace CatchTheWord
{
    public class CScore
    {
        private CGame Main;
        GraphicsDevice GraphicsDevice;
        CButton backButton;
        public HighScoreData ScoreData;

        public void Init()
        {
            Texture2D backImage = Main.Content.Load<Texture2D>("bt_back");
            int x_offset = 5;
            int y_offset = GraphicsDevice.Viewport.Height - backImage.Height - 5;
            backButton = new CButton(x_offset, y_offset, backImage);
            backButton.ButtonPressed += new EventHandler(BackButton_Click);

        }

        public CScore(CGame game1, GraphicsDevice gd)
        {

            Main = game1;
            GraphicsDevice = gd;
        }

        public void Update(GameTime gameTime)
        {
            backButton.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.Content.Load<Texture2D>("ScoreContent/bg_score"), new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);

            backButton.Draw(spriteBatch);

            spriteBatch.Draw(Main.Content.Load<Texture2D>("ScoreContent/bg_scroll"), new Rectangle(250, 100, 300, 444), Color.White);
            CGame.DrawString(spriteBatch, CGame.Andy25, "Score", new Rectangle(250, 150, 300, 444), CGame.EAlignment.Top, Color.Black);
            spriteBatch.DrawString(CGame.Andy10, "No.", new Vector2(275, 185), Color.Black);
            spriteBatch.DrawString(CGame.Andy10, "Name", new Vector2(300, 185), Color.Black);
            spriteBatch.DrawString(CGame.Andy10, "Score", new Vector2(400, 185), Color.Black);
            spriteBatch.DrawString(CGame.Andy10, "Date", new Vector2(470, 185), Color.Black);

            int linespace = 25;
            for (int i = 0; i < 10; i++)
            {
                int y_pos = 210 + (i * linespace);
                spriteBatch.DrawString(CGame.Andy10, (i+1) + ".", new Vector2(284, y_pos), Color.Black);
                spriteBatch.DrawString(CGame.Andy10, ScoreData.PlayerName[i], new Vector2(300, y_pos), Color.Black);
                spriteBatch.DrawString(CGame.Andy10, ScoreData.Score[i].ToString(), new Vector2(400, y_pos), Color.Black);
                spriteBatch.DrawString(CGame.Andy10, ScoreData.Date[i].ToString("d"), new Vector2(470, y_pos), Color.Black);
            }


        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            Main.CurrentGameState = CGame.EGameState.MainMenu;
        }

        public static void InitialHighScores()
        {
            // Get the path of the save game
            string fullpath = Path.Combine(Environment.CurrentDirectory, "save.dat");

            // Check to see if the save exists
            if (!File.Exists(fullpath))
            {
                //If the file doesn't exist, make a fake one...
                // Create the data to save
                HighScoreData data = new HighScoreData(10);
                for (int i = 0; i < 10; i++)
                {
                    data.PlayerName[i] = "Player" + (i + 1);
                    data.Score[i] = 10 - 1 * i;
                    data.Date[i] = DateTime.Now;
                }

                SaveHighScores(data);
            }
        }

        public static void SaveHighScores(HighScoreData data)
        {
            // Get the path of the save game
            string fullpath = Path.Combine(Environment.CurrentDirectory, "save.dat");

            // Open the file, creating it if necessary
            FileStream stream = File.Open(fullpath, FileMode.OpenOrCreate);
            try
            {
                // Convert the object to XML data and put it in the stream
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                serializer.Serialize(stream, data);
            }
            finally
            {
                // Close the file
                stream.Close();
            }
        }

        public static HighScoreData LoadHighScores()
        {
            HighScoreData data;

            // Get the path of the save game
            string fullpath = Path.Combine(Environment.CurrentDirectory, "save.dat");

            // Open the file
            FileStream stream = File.Open(fullpath, FileMode.OpenOrCreate,
            FileAccess.Read);
            try
            {

                // Read the data from the file
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                data = (HighScoreData)serializer.Deserialize(stream);
            }
            finally
            {
                // Close the file
                stream.Close();
            }

            return (data);
        }

        public void SaveHighScore(string player, int score)
        {
            // Create the data to save
            HighScoreData data = LoadHighScores();

            int scoreIndex = -1;
            for (int i = 0; i < data.Count; i++)
            {
                if (score > data.Score[i])
                {
                    scoreIndex = i;
                    break;
                }
            }

            if (scoreIndex > -1)
            {
                //New high score found ... do swaps
                for (int i = data.Count - 1; i > scoreIndex; i--)
                {
                    data.PlayerName[i] = data.PlayerName[i - 1];
                    data.Date[i] = data.Date[i - 1];
                    data.Score[i] = data.Score[i - 1];
                }

                data.PlayerName[scoreIndex] = player; //Retrieve User Name Here
                data.Date[scoreIndex] = DateTime.Now;
                data.Score[scoreIndex] = score;

                SaveHighScores(data);
            }
        }
    }
}
